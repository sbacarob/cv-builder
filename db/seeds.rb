# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
require 'csv'

if Country.first.blank?
  csv_text = File.read(Rails.root.join('lib', 'seeds', 'countries.csv'))
  csv = CSV.parse(csv_text, :headers => true, :encoding => 'ISO-8859-1')
  csv.each do |row|
    c = Country.new
    c.name = row['Name']
    c.code = row['Code']
    c.save
    puts "#{c.name} saved"
  end

  puts "There are now #{Country.count} rows in the countries table"
end

if Region.first.blank?
  csv_text = File.read(Rails.root.join('lib', 'seeds', 'world_cities.csv'))
  csv = CSV.parse(csv_text, :headers => true, :encoding => 'ISO-8859-1')
  csv.each do |row|
    r = Region.new
    c = City.new
    c.name = row['name']
    r.name = row['subcountry']
    country_code = Country.where(name: row['country']).try(:first).try(:code)
    c.country_code = country_code
    r.country_code = country_code

    re = Region.where(name: r.name, country_code: r.country_code).first_or_create
    puts "region #{re.name} passed"

    begin
      c.save!
      puts "city #{c.name} saved"
    rescue
      puts "city #{c.name} already existed"
    end
  end
end
