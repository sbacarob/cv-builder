# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180907032358) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "cities", force: :cascade do |t|
    t.string "name"
    t.string "country_code"
    t.string "region_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name", "country_code", "region_name"], name: "index_cities_on_name_and_country_code_and_region_name", unique: true
  end

  create_table "countries", force: :cascade do |t|
    t.string "name"
    t.string "code"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "custom_sections", force: :cascade do |t|
    t.bigint "user_id"
    t.string "title"
    t.string "icon"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_custom_sections_on_user_id"
  end

  create_table "cv_sections", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "cv_id"
    t.bigint "section_id"
    t.text "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["cv_id"], name: "index_cv_sections_on_cv_id"
    t.index ["section_id"], name: "index_cv_sections_on_section_id"
    t.index ["user_id"], name: "index_cv_sections_on_user_id"
  end

  create_table "cvs", force: :cascade do |t|
    t.bigint "user_id"
    t.integer "template_id"
    t.integer "language_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_cvs_on_user_id"
  end

  create_table "regions", force: :cascade do |t|
    t.string "country_code"
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["country_code", "name"], name: "index_regions_on_country_code_and_name", unique: true
  end

  create_table "sections", force: :cascade do |t|
    t.string "title"
    t.string "icon"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "user_profiles", force: :cascade do |t|
    t.bigint "user_id"
    t.string "second_name"
    t.string "other_names"
    t.string "photo_url"
    t.string "address_1"
    t.string "address_2"
    t.string "postcode"
    t.string "land_phone"
    t.string "city_code"
    t.string "country_code"
    t.string "area_code"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_user_profiles_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email"
    t.string "password_digest"
    t.string "first_name"
    t.string "last_names"
    t.string "mobile_phone"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "phone_prefix"
  end

  add_foreign_key "custom_sections", "users"
  add_foreign_key "cv_sections", "cvs"
  add_foreign_key "cv_sections", "sections"
  add_foreign_key "cv_sections", "users"
  add_foreign_key "cvs", "users"
  add_foreign_key "user_profiles", "users"
end
