class CreateRegions < ActiveRecord::Migration[5.1]
  def change
    create_table :regions do |t|
      t.string :country_code
      t.string :name

      t.timestamps
    end

    add_index :regions, [:country_code, :name], :unique => true
  end
end
