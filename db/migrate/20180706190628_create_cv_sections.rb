class CreateCvSections < ActiveRecord::Migration[5.1]
  def change
    create_table :cv_sections do |t|
      t.references :user, foreign_key: true
      t.references :cv, foreign_key: true
      t.references :section, foreign_key: true
      t.text :content

      t.timestamps
    end
  end
end
