class CreateCvs < ActiveRecord::Migration[5.1]
  def change
    create_table :cvs do |t|
      t.references :user, foreign_key: true
      t.integer :template_id
      t.integer :language_id

      t.timestamps
    end
  end
end
