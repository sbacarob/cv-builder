class CreateCustomSections < ActiveRecord::Migration[5.1]
  def change
    create_table :custom_sections do |t|
      t.references :user, foreign_key: true
      t.string :title
      t.string :icon

      t.timestamps
    end
  end
end
