class CreateCities < ActiveRecord::Migration[5.1]
  def change
    create_table :cities do |t|
      t.string :name
      t.string :country_code
      t.string :region_name

      t.timestamps
    end

    add_index :cities, [:name, :country_code, :region_name], :unique => true
  end
end
