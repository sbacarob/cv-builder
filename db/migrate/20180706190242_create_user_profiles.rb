class CreateUserProfiles < ActiveRecord::Migration[5.1]
  def change
    create_table :user_profiles do |t|
      t.references :user, foreign_key: true
      t.string :second_name
      t.string :other_names
      t.string :photo_url
      t.string :address_1
      t.string :address_2
      t.string :postcode
      t.string :land_phone
      t.string :city_code
      t.string :country_code
      t.string :area_code

      t.timestamps
    end
  end
end
