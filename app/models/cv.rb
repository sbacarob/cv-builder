class Cv < ApplicationRecord
  belongs_to :user
  has_many :cv_sections
end
