class User < ApplicationRecord
  EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence: true, length: { maximum: 255 },
                    format: { with: EMAIL_REGEX },
                    uniqueness: { case_sensitive: false }

  validates :password, presence: true, length: { minimum: 8 }, allow_nil: true

  before_save { email.downcase! }

  has_many :cv_sections
  has_many :cvs
  has_one  :user_profile
  has_many :custom_sections

  has_secure_password
end
