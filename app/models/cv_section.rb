class CvSection < ApplicationRecord
  belongs_to :user
  belongs_to :cv
  belongs_to :section
end
