class RegionsController < ApplicationController

  def list
    @results = Region.where(country_code: params[:country_id]).map{ |x| x.name }
    render :json => @results
  end
end
