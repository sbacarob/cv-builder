class UsersController < ApplicationController

  def index
  end

  def show
    @user = User.find(params[:id])
  end

  def new
    @user = User.new
  end

  def update
    @user = User.find(session[:user_id])
    if @user.update_attributes(user_params)
      flash[:success] = "User info updated succesfully."
      redirect_to user_profile_path(@user)
    else
      flash[:error] = "#{@user.errors.keys[0].to_s} #{@user.errors.values[0][0]}".capitalize
      redirect_to user_profile_path(@user)
    end
  end

  def create
    @user = User.new(user_params)

    if @user.save
      log_in @user
      flash[:success] = "Your account was succesfully created."
      redirect_to @user
    else
      render 'new'
    end
  end


  private

    def user_params
      params.require(:user).permit(:email, :password, :password_confirmation, :phone_prefix, :first_name, :last_names, :mobile_phone)
    end
end
