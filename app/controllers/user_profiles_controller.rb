class UserProfilesController < ApplicationController
  def new
    @user = User.find(params[:user_id])
    @user_profile = @user.user_profile
  end

  def create
    @user = User.find(params[:user_id])
    @user_profile = @user.build_user_profile(user_profile_params)
    if @user_profile.save
      flash[:success] = "Your profile info was succesfully updated."
      redirect_to @user
    else
      flash[:error] = "#{@user_profile.errors.keys[0].to_s} #{@user_profile.errors.values[0][0]}".capitalize
      redirect_to user_profile_path(@user)
    end
  end

  def update
    @user = User.find(params[:user_id])
  end

  def show
    @user = User.find(params[:user_id])
  end

  private

    def user_profile_params
      params.require(:user_profile).permit(:second_name, :other_names,
                                           :address_1, :address_2, :postcode,
                                           :land_phone, :country_code,
                                           :area_code, :city_code)
    end
end
