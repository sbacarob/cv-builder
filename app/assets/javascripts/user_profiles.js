// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.
// You can use CoffeeScript in this file: http://coffeescript.org/

$(document).ready(function(){
  $("#user_profile_country_code").change(function(){

    code = $(this).val();

    $.get("/"+ code + "/regions", function(data){
        console.log(data);
        $("#user_profile_area_code").find('option').remove().end()
        for(var i = 0; i < data.length; i++){
          $("#user_profile_area_code")
            .append('<option value="' + data[i] + '">' + data[i] + '</option>')
        }
        $("#user_profile_area_code").removeAttr("disabled")
      });
  });
});
