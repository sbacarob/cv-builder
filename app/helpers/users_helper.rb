module UsersHelper

  def user_location(user)
    if user.present? && user.user_profile.present?
      code = user.user_profile.country_code
      city = user.user_profile.city_code
      country = code.present? ? Country.where(code: code).first.name : ''
      return "#{city}, #{country}".strip()
    end
    ''
  end

  def user_cvs_count(user)
    if user.present?
      count = user.cvs.count
      return "#{pluralize(count, 'created CV')}"
    end
  end

  def user_sections_count(user)
    if user.present?
      count = user.cv_sections.count
      return "#{pluralize(count, 'filled section')}"
    end
  end

  def user_custom_sections_count(user)
    if user.present?
      count = user.custom_sections.count
      return "#{pluralize(count, 'custom section')}"
    end
  end
end
