Rails.application.routes.draw do
  get 'cvs/new'

  get 'cvs/create'

  get 'cvs/index'

  get 'cvs/edit'

  get 'regions/list'

  get 'user_profiles/new'

  get 'user_profiles/update'

  get 'user_profiles/show'

  root to: 'sessions#new'
  get 'sessions/new'

  # Users routes
  get '/signup', to: 'users#new'
  post '/signup', to: 'users#create'

  # Sessions managing
  get    '/login',  to: 'sessions#new'
  post   '/login',  to: 'sessions#create'
  delete '/logout', to: 'sessions#destroy'

  get '/:country_id/regions', to: 'regions#list'

  resources :users do
    resources :cvs

    get   '/profile', to: 'user_profiles#new'
    post  '/profile', to: 'user_profiles#create'
    patch '/update',  to: 'users#update'
  end
end
